'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
 	uglify = require('gulp-uglify'),
 	concat = require('gulp-concat');

gulp.task('sass', function () {
  gulp.src('./resources/scss/*.scss')
        .pipe(sass())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'))
    .pipe(minifycss())
    .pipe(rename('jcmix.min.css'))
    .pipe(gulp.dest('./assets/css'));
});
gulp.task('default', function () {
  gulp.watch('./resources/scss/*.scss', ['sass']);
  gulp.watch('./resources/scss/**/*.scss', ['sass']);
  gulp.watch('./resources/js/jcmix.js', ['scripts']);
});

gulp.task('scripts', function() {
  return gulp.src(['./resources/js/plugins/recorder.js','./resources/js/plugins/jquery.knob.min.js','./resources/js/jcmix.js'])
    .pipe(concat({ path: 'jcmix.js' }))
    .pipe(gulp.dest('./assets/js'))
    .pipe(uglify({ mangle:false }))
    .pipe(rename('jcmix.min.js'))
    .pipe(gulp.dest('./assets/js'));
});

